import os
from os import listdir
from os.path import isfile, join
import pandas as pd
from LogFile import Logging
from UsersList import GetUserList

# Base directories and files
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# Processing of response data from the managers
def ProcessResponseData():
    RESPONSE_EMAIL_DATA = os.path.join(BASE_DIR, "ResponseData")
    response_data = pd.DataFrame(columns=['FIRST_NAME','LAST_NAME','NT_LOGIN','UP1_MANAGER','ACCOUNT_CREATED_DATE','DAYS_INACTIVE_LAST',
    'IPACT_AD_RECORD_STATUS','Disable'])
    onlyfiles = [f for f in listdir(RESPONSE_EMAIL_DATA) if isfile(join(RESPONSE_EMAIL_DATA, f))]
    for file in onlyfiles:
        data = pd.read_csv(os.path.join(RESPONSE_EMAIL_DATA, file))
        response_data = response_data.append(data)
    Logging().get_logger().info("Retrieved the response data from every file")
    response_data.set_index("IPACT_USER_ID", inplace=True)
    users_diabled = response_data.loc[response_data['Disable'] == 'YES']
    total_inactive_users = GetUserList().get_inactive_users()
    indexes = list(set(response_data.index).symmetric_difference(total_inactive_users.index))
    finalProcess(total_inactive_users.loc[indexes], users_diabled)

# Storing the response data into the FinalDisbaledUsers folder in a file
def finalProcess(unresponse_inactive_users , users_diabled):
    FRESH_RESPONSE_EMAIL_DATA = os.path.join(BASE_DIR, "fresh_data/UAR.csv")
    FINAL_DISABLED_USERS = os.path.join(BASE_DIR, "FinalDisabledUsers")
    fresh_data = pd.read_csv(FRESH_RESPONSE_EMAIL_DATA)
    fresh_data.set_index("IPACT_USER_ID", inplace=True)
    inactive_users = (fresh_data.loc[unresponse_inactive_users.index].DAYS_INACTIVE_LAST > 90) & (
        fresh_data.loc[unresponse_inactive_users.index].IPACT_AD_RECORD_STATUS.str.contains("CURRENT"))
    finalized_unresponse_inactive_users = unresponse_inactive_users[inactive_users]
    finalized_unresponse_inactive_users = finalized_unresponse_inactive_users.append(users_diabled)
    csv_store = os.path.join(FINAL_DISABLED_USERS, "disabledUsers.csv")
    finalized_unresponse_inactive_users.to_csv(csv_store, index=True)


    

#importing module 
import logging 

class Logging:
    '''
    classDocs
    '''
    def __init__(self):
        '''
        Constructor
        '''
        #Create and configure logger 
        logging.basicConfig(filename="log_data.log", 
                            format='%(asctime)s %(message)s', 
                            filemode='w') 

        #Creating an object 
        self.logger = logging.getLogger()
        #Setting the threshold of logger to INFO
        self.logger.setLevel(logging.INFO) 
        
    # return logger
    def get_logger(self):
        return self.logger
    

import os
from datetime import datetime
from threading import Timer
from UsersList import GetUserList
from EmailManager import send_email_to_managers
from ProcessResponseData import ProcessResponseData
from LogFile import Logging

if __name__ == "__main__":
    '''
    main
    '''
    logger = Logging().get_logger()
    # setting the start and end date to trigger the flow down function
    x=datetime.today()
    logger.info("Start Date %s",str(x))
    # get_user_list object initialization where the initial query or file reading happen
    users_list = GetUserList()
    # get the users list
    users_list_dataframe = users_list.getData()
    # get the inactive users list
    inactive_users = users_list.get_inactive_users()
    # get the users grouped by managers
    users_data_grouped_by_managers = users_list.getDataGroupyByManager(inactive_users)
    # send email to managers
    send_email_to_managers(users_data_grouped_by_managers , users_list_dataframe)
    # set the time to trigger finalProcess functions after stiputated time period, here 3 days
    y=x.replace(day=x.day, hour=x.hour, minute=x.minute, second=x.second+5, microsecond=0)
    # converting time into secs
    delta_t=y-x
    secs=delta_t.seconds+1
    logger.info("End Date After 3 days of time period")
    # triggering the final function to generate the final users list disabled
    t = Timer(secs, ProcessResponseData)
    t.start()
import pandas as pd
import os
from LogFile import Logging

class GetUserList :
    '''
    classDocs
    '''
    def __init__(self):
        '''
        constructor
        '''
        # Base directories and files
        self.BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        self.DATA = os.path.join(self.BASE_DIR, "data/UAR.csv")
        self.users = pd.read_csv(self.DATA, usecols=lambda column: column not in [
            "AD_ACCESS_STATE", "AD_DEPARTMENT_NAME", "LAST_ACCESS_TIME", "DEACTIVATION_REQUIRED", "DEACTIVATION_ALERT_REQUIRED"])
        self.users.set_index("IPACT_USER_ID", inplace=True)
        self.users["Disable"] = "YES"
        self.logger = Logging()
    
    # get user details
    def getData(self):
        self.logger.get_logger().info("Got Users List")
        return self.users

    # get inactive users list
    def get_inactive_users(self):
        current_users = (self.users.DAYS_INACTIVE_LAST > 90) & (
        self.users.IPACT_AD_RECORD_STATUS.str.contains("CURRENT"))
        inactive_users = self.users[current_users]
        self.logger.get_logger().info("Got Inactive Users List")
        return inactive_users

    # get data grouped by managers
    def getDataGroupyByManager(self, data):
        managers_id = []
        upi_managers = data.groupby(['UP1_MANAGER']).groups
        managers = list(upi_managers.keys())
        for manager in managers:
            ls = []
            ls.append(manager)
            ls.append(upi_managers[manager].values.tolist())
            managers_id.append(tuple(ls))
        self.logger.get_logger().info("Got Inactive Users List grouped by manager")
        return tuple(managers_id)
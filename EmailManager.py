import win32com.client as win32
import os
from LogFile import Logging

# Base directories and files
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
EMAILDATA = os.path.join(BASE_DIR, "EmailData")
logger = Logging().get_logger()

 # Email Format, uncomment after installing the packages in requirement.txt file
'''
def emailTemplate(manager, attachment):
        # -- To Names
        to = str(manager)+'@gmail.com'
        name = "Hi " + manager + ","
        # -- CC Names
        # -- Set a Subject
        subject = "IPACT Inactive Users List"
        # -- Set a Signature
        signature = """<html><body> <p style=" font-family: Calibri; font-size: 90%;">Regards,<br>""" + manager + """,<br>
    Bangalore.</p></body></html>"""
        # -- Set a attachment
        attachment_path = attachment
        content = """<html><body>
                            <style>
                            table, th, td {
                                border: 1px solid black;
                                border-collapse: collapse;
                                font-family: Calibri;
                                font-size: 16px;
                                }
                            th{
                                background-color:DodgerBlue;
                            }    
                            </style>
                            <p style=" font-family: Calibri; font-size: 90%;">""" + manager + """<br>
                            <br>
                                Please find attached, IPACT Inactive Users List.<br></p></body></html>"""

        # -- Create Outlook Object
        outlook = win32.Dispatch('outlook.application')
        # -- Create Outlook Item for composing mail
        mail = outlook.CreateItem(0)
        # -- Create attachment object and path for it
        mail.Attachments.Add(attachment_path)
        # -- Mail Subject
        mail.Subject = subject
        # -- Mail body
        mail.HTMLBody = content + signature
        # -- Mail address to "TO" field
        mail.To = to
        # mail.Bcc =

        # print("Mail sent!")
        mail.Send()
        '''
    

# function to send emails to managers
def send_email_to_managers(managers_list , users_list):
    for groups in managers_list:
        manager , user_ids = groups
        user_data = users_list.loc[user_ids]
        csv_store = os.path.join(EMAILDATA, ""+manager+".csv")
        user_data.to_csv(csv_store, index = True)
        logger.info("Email sent to manager %s", manager)
        '''
        function call to emailTemplate, html email template format.
        emailTemplate(manager , csv_store)
        '''
        
        